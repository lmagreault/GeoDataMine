--METADATA={ "name:fr": "Banc", "name:en": "Bench", "theme:fr": "Infrastructures", "keywords:fr": [ "banc" ], "description:fr": "Bancs issus d'OpenStreetMap (amenity=bench)" }
--GEOMETRY=point,line

SELECT
	<OSMID> as osm_id,
	t.tags->'backrest' AS backrest,
	t.tags->'material' AS material,
	t.tags->'seats' AS seats,
	t.tags->'colour' AS colour,
	t.tags->'bench' AS bench,
	t.tags->'direction' AS direction,
	t.tags->'operator' AS operator,
	t.access,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity = 'bench' AND <GEOMCOND>
