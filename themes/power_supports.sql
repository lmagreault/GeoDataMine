--METADATA={ "name:fr": "Supports aériens électriques/telecoms", "name:en": "Utility supports", "theme:fr": "Infrastructures", "keywords:fr": [ "énergie", "telecoms", "réseaux", "poteaux", "pylônes" ], "description:fr": "Supports des réseaux électriques et télécoms aériens issus d'OpenStreetMap (power=pole,power=tower ou man_made=utility_pole)" }
--GEOMETRY=point

SELECT
	<OSMID> as osm_id,
	COALESCE(t.tags->'utility', CASE
		WHEN t.power IN ('pole','tower') THEN 'power'
		ELSE NULL END)
		AS utility,
	CASE
		WHEN t.power IN ('pole','tower') THEN t.power
		WHEN t.man_made = 'utility_pole' THEN 'pole'
		ELSE NULL END
		AS nature,
	t.tags->'operator' AS operator,
	t.tags->'material' AS material,
	t.tags->'height' AS height,
	t.tags->'ref' AS reference,
	t.tags->'line_attachment' AS line_attachment,
	t.tags->'line_management' AS line_management,
	t.tags->'location:transition' AS transition,
	<ADM8REF> AS com_insee,
	<ADM8NAME> AS com_nom,
	substring(tags->'osm_timestamp', 1, 10) AS last_update,
	<GEOM>
FROM <TABLE>
WHERE (t.power IN ('pole','tower') OR t.man_made = 'utility_pole') AND <GEOMCOND>
