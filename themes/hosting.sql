--METADATA={ "name:fr": "Hébergement touristique", "name:en": "Hosting for tourists", "theme:fr": "Tourisme", "keywords:fr": [ "hôtel", "chalet", "camping", "camping-car", "chambre d'hôtes" ], "description:fr": "Hébergements touristiques issus d'OpenStreetMap (tourism=hotel|chalet|guest_house|apartment|hostel|motel|wilderness_hut|alpine_hut|camp_site|caravan_site)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.tourism AS type, t.tags->'name' AS name,
	t.tags->'operator' AS operator, t.wheelchair, t.tags->'opening_hours' AS opening_hours,
	t.tags->'opening_hours:reception' AS reception_opening_hours, t.tags->'stars' AS stars,
	t.tags->'rooms' AS rooms, t.tags->'beds' AS beds, t.internet_access, t.tags->'camp_site' AS camp_site,
	t.tags->'fee' AS fee, t.tags->'capacity' AS capacity,
	t.tags->'website' AS website, t.tags->'phone' AS phone, t.tags->'facebook' AS facebook, t.tags->'email' AS email,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.tourism IN ('hotel', 'chalet', 'guest_house', 'apartment', 'hostel', 'motel', 'wilderness_hut', 'alpine_hut', 'camp_site', 'caravan_site') AND <GEOMCOND>
