--METADATA={ "name:fr": "Table de pique-nique", "name:en": "Picnic table", "theme:fr": "Loisir", "keywords:fr": [ "table de pique-nique" ], "description:fr": "Tables de pique-nique issues d'OpenStreetMap (leisure=picnic_table)" }
--GEOMETRY=point

SELECT
	<OSMID> as osm_id,
	t.tags->'covered' AS covered,
	t.tags->'material' AS material, 
	t.tags->'bench' AS bench,
	t.tags->'backrest' AS backrest,
	t.tags->'lit' AS lit,
	t.tags->'colour' AS colour,
	t.wheelchair,
	t.tags->'direction' AS direction,
	t.tags->'operator' AS operator,
	t.access,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.leisure = 'picnic_table' AND <GEOMCOND>
