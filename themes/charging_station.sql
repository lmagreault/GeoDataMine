--METADATA={ "name:fr": "Recharge de véhicule électrique (schéma Étalab IRVE 2.0.0)", "name:en": "Vehicle charging station", "theme:fr": "Transports", "keywords:fr": [ "irve", "borne de recharge électrique" ], "description:fr": "Bornes de recharge de véhicule électrique issues d'OpenStreetMap (amenity=charging_station) au format Schéma IRVE", "skipGeomCSV": true }
--GEOMETRY=point,polygon

SELECT
	t.tags->'owner' AS nom_amenageur,
	t.tags->'owner:ref:FR:SIREN' AS siren_amenageur,
	t.tags->'owner:email' AS contact_amenageur,
	t.tags->'operator' AS nom_operateur,
	COALESCE(t.tags->'operator:email', t.tags->'contact:email', t.tags->'email') AS contact_operateur,
	COALESCE(t.tags->'operator:phone', t.tags->'contact:phone', t.tags->'phone') AS telephone_operateur,
	COALESCE(t.tags->'network', t.tags->'brand') AS nom_enseigne,
	'Non concerné' AS id_station_itinerance,
	t.tags->'ref' AS id_station_local,
	array_to_string(array_remove(ARRAY[
		CASE WHEN t.tags->'ref' IS NOT NULL THEN  t.tags->'ref' ELSE '' END,
		CASE WHEN t.tags->'name' IS NOT NULL THEN t.tags->'name' ELSE '' END
	], ''), ' - ') AS nom_station,
	'' AS implantation_station,
	CASE
		WHEN t.tags->'addr:housenumber' IS NOT NULL AND t.tags->'addr:street' IS NOT NULL THEN concat(t.tags->'addr:housenumber', ' ', t.tags->'addr:street', ', ', <ADM8NAME>)
	END AS adresse_station,
	<ADM8REF> AS code_insee_commune,
	CONCAT('[', ST_X(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)), ',', ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)), ']') AS "coordonneesXY",
	t.tags->'capacity' AS nbre_pdc,
	'Non concerné' AS id_pdc_itinerance,
	t.tags->'ref:EU:EVSE' AS id_pdc_local,
	CASE WHEN t.tags->'charging_station:output' IS NOT NULL THEN replace(t.tags->'charging_station:output', ' kW', '') END AS puissance_nominale,
	CASE WHEN (t.tags->'socket:typee' IS NOT NULL AND t.tags->'socket:typee' != 'no') OR (t.tags->'socket:schuko' IS NOT NULL AND t.tags->'socket:schuko' != 'no') THEN 'true' ELSE '' END AS prise_type_ef,
	CASE WHEN t.tags->'socket:type2' IS NOT NULL AND t.tags->'socket:type2' != 'no' THEN 'true' ELSE '' END AS prise_type_2,
	CASE WHEN t.tags->'socket:type2_combo' IS NOT NULL AND t.tags->'socket:type2_combo' != 'no' THEN 'true' ELSE '' END AS prise_type_combo_ccs,
	CASE WHEN t.tags->'socket:chademo' IS NOT NULL AND t.tags->'socket:chademo' != 'no' THEN 'true' ELSE '' END AS prise_type_chademo,
	'' AS prise_type_autre,
	CASE
		WHEN t.tags->'fee' = 'yes' THEN 'true'
		WHEN t.tags->'fee' = 'no' THEN 'false'
	END AS gratuit,
	'' AS paiement_acte,
	CASE
		WHEN t.tags->'payment:credit_cards' = 'yes' OR t.tags->'payment:cb' = 'yes' OR t.tags->'payment:maestro' = 'yes' OR t.tags->'payment:mastercard' = 'yes' OR t.tags->'payment:visa' = 'yes' OR t.tags->'payment:american_express' = 'yes' THEN 'true'
		WHEN t.tags->'payment:credit_cards' = 'no' THEN 'false'
	END AS paiement_cb,
	CASE
		WHEN t.tags->'payment:coins' = 'yes' OR t.tags->'payment:notes' = 'yes' OR t.tags->'payment:cash' = 'yes' OR t.tags->'payment:sms' = 'yes' OR t.tags->'payment:toll_number' = 'yes' OR t.tags->'payment:mobile_phone' = 'yes' OR t.tags->'payment:navigo' = 'yes' OR t.tags->'payment:korrigo' = 'yes' THEN 'true'
	END AS paiement_autre,
	COALESCE(t.tags->'charge', t.tags->'fee:amount', t.tags->'fee:price') AS tarification,
	CASE
		WHEN t.tags->'access' IN ('private', 'customers') THEN 'Accès réservé'
		ELSE 'Accès libre'
	END AS condition_acces,
	CASE
		WHEN t.tags->'reservation' IN ('required', 'recommended', 'yes', 'members_only') THEN 'true'
	END AS reservation,
	t.tags->'opening_hours' AS "horaires",
	CASE
		WHEN t.tags->'wheelchair' = 'yes' THEN 'Accessible mais non réservé PMR'
		WHEN t.tags->'wheelchair' = 'no' THEN 'Non accessible'
		WHEN t.tags->'access' = 'no' AND t.tags->'disabled' IN ('yes', 'designated') THEN 'Réservé PMR'
		ELSE 'Accessibilité inconnue'
	END AS accessibilite_pmr,
	array_to_string(array_remove(ARRAY[
		CASE WHEN t.tags->'maxheight' IS NOT NULL THEN CONCAT('Hauteur max ', t.tags->'maxheight') ELSE '' END,
		CASE WHEN t.tags->'maxwidth' IS NOT NULL THEN CONCAT('Longueur max ', t.tags->'maxwidth') ELSE '' END,
		CASE WHEN t.tags->'maxweight' IS NOT NULL THEN CONCAT('Poids max ', t.tags->'maxweight') ELSE '' END
	], ''), ', ') AS restriction_gabarit,
	CASE
		WHEN t.tags->'bicycle' = 'yes' OR t.tags->'scooter' = 'yes' THEN 'true'
		WHEN t.tags->'bicycle' = 'no' AND t.tags->'scooter' = 'no' THEN 'false'
	END AS station_deux_roues,
	'' AS raccordement,
	'' AS num_pdl,
	t.tags->'start_date' AS date_mise_en_service,
	array_to_string(array_remove(ARRAY[
		<OSMID>,
		CASE WHEN t.tags->'description' IS NOT NULL THEN t.tags->'description' ELSE '' END
	], ''), ', ') AS observations,
	split_part(t.tags->'osm_timestamp', 'T', 1) AS date_maj,
	<GEOM>
FROM <TABLE>
WHERE t.amenity = 'charging_station' AND <GEOMCOND>
