--METADATA={ "name:fr": "Point d'accès Internet", "name:en": "Internet access point", "theme:fr": "Infrastructures", "keywords:fr": [ "point d'accès internet", "wifi", "internet" ], "description:fr": "Établissements disposant d'un accès Internet (internet_access=*)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.internet_access AS connexion_type, t.tags->'internet_access:fee' AS fee,
	COALESCE(t.tags->'internet_access:description', t.tags->'description') AS description,
	t.tags->'internet_access:operator' AS internet_operator, t.tags->'internet_access:ssid' AS ssid,
	t.tags->'name' AS place_name, t.access AS place_access, t.tags->'opening_hours' AS place_opening_hours,
	COALESCE(t.amenity, t.shop, t.tourism, t.office, 'unknown') AS place_type,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE internet_access IS NOT NULL AND internet_access != 'no' AND <GEOMCOND>
