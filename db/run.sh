#!/bin/bash

##############################################
#                                            #
# Download and import raw OpenStreetMap data #
#                                            #
##############################################

# Start date (for logs)
date

# Read configuration from JSON file
echo "======= Preparing ======="
for s in $(cat "../config.json" | jq -r "to_entries|map(select(.key != \"LICENSE\"))|map(\"\(.key)=\(.value|tostring)\")|.[]" ); do
	export $s
done

start=`date +%s`

# Create work directory if not exists
if ! mkdir -p "${WORK_DIR}"; then
	echo "Can't create work directory"
	exit 1
fi


# PBF extract download
last_ts_file="${WORK_DIR}/last_ts.txt"
yesterday_ts=`date +%s -d '-20 hours'`
pbf_file="${WORK_DIR}/extract.osm.pbf"
pbf_light_file="${WORK_DIR}/extract_light.osm.pbf"

# We check how old is the latest extract to avoid over-downloading
echo "======= OSM data downloading ======="
if ! [ -f "${last_ts_file}" -a -f "${last_ts_file}" -a `if [ -f "${last_ts_file}" ]; then cat "${last_ts_file}"; else echo "0"; fi` -gt "${yesterday_ts}" ]; then
	# Start download
	if wget "${PBF_URL}" --quiet -O "${pbf_file}"; then
		# Success -> Write TS file
		date +%s > "${last_ts_file}"
		rm -f "${pbf_light_file}"
		echo "Success"
	else
		# Failure -> Stop script
		echo "Can't download file"
		rm -f "${pbf_file}" "${pbf_light_file}" "${last_ts_file}"
		exit 1
	fi
else
	echo "OSM extract recent enough, skip download"
fi

set -e

# Pre-filter PBF file to only import what's necessary
echo "======= Filtering input PBF data (osmium) ======="
used_tags="`cat flex_rules.lua | grep "{ column =" | cut -d "'" -f 2 | grep -v -E "^(way|area|rel_tags|tags|node_id)$" | sort | uniq` type"

if ! [ -f "${pbf_light_file}" ]; then
	osmium tags-filter \
		${pbf_file} \
		${used_tags} \
		-f pbf,add_metadata=version+timestamp \
		--no-progress \
		--overwrite -o ${pbf_light_file}
	echo "Success"
else
	echo "Reuse existing light PBF file"
fi

# Setup next database
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "DROP DATABASE IF EXISTS ${PG_DB_TMP}"
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "CREATE DATABASE ${PG_DB_TMP}"
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${PG_DB_TMP}" -c "CREATE EXTENSION postgis; CREATE EXTENSION hstore"
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "DROP DATABASE IF EXISTS ${PG_DB_OLD}"


# Start import
echo "======= Import OSM data into DB ======="
flat_nodes="${WORK_DIR}/flat.nodes"
flat_nodes_opts=""

if [ "${FLAT_NODES}" == "true" ]; then
	flat_nodes_opts="--flat-nodes ${flat_nodes}"
	echo "Flat nodes option enabled"
fi

osm2pgsql --create \
	-H "${PG_HOST}" -U "${PG_USER}" -P "${PG_PORT}" -d "${PG_DB_TMP}" \
	--cache "${CACHE_MEM}" \
	--slim --drop \
	${flat_nodes_opts} \
	--number-processes "${PARALLEL_JOBS}" \
	--extra-attributes \
	--style "flex_rules.lua" \
	--output=flex \
	"${pbf_light_file}"

# Create indexes for all tables
echo "======= Post-processing ======="
set +e
for col in ${used_tags}; do
	echo "Create index for $col"
	psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${PG_DB_TMP}" -c "CREATE INDEX planet_osm_point_${col//:/_}_idx ON planet_osm_point(\"$col\")"
	psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${PG_DB_TMP}" -c "CREATE INDEX planet_osm_line_${col//:/_}_idx ON planet_osm_line(\"$col\")"
	psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${PG_DB_TMP}" -c "CREATE INDEX planet_osm_polygon_${col//:/_}_idx ON planet_osm_polygon(\"$col\")"
done
set -e

# Launch post-process script
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${PG_DB_TMP}" -f post_process.sql

# JSON boundary version for API
bounds_json="
SELECT
  array_to_json(array_agg(json_build_object(
    'id', b.osm_id,
    'name', b.name,
    'ref', b.ref,
    'type', b.type
  ))) AS data
FROM boundary b;
"


# Alert API that database will be unavailable
echo "Stop API queries to update database"
lock_file="${WORK_DIR}/db.lock"
echo "lock" > "${lock_file}"
sleep 30

# Change bounds
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${PG_DB_TMP}" -t -A -c "${bounds_json}" > "${WORK_DIR}/bounds.json"

# Rename databases to make next goes live
echo "======= Switch next with current DB ======="
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND datname='${PG_DB_CLEAN}'"
set +e
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${PG_DB_TMP}" -c "ALTER DATABASE ${PG_DB_CLEAN} RENAME TO ${PG_DB_OLD}"
set -e
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND datname='${PG_DB_TMP}'"
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "ALTER DATABASE ${PG_DB_TMP} RENAME TO ${PG_DB_CLEAN}"

# Unlock API
rm "${lock_file}"

# Cleanup
psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "DROP DATABASE IF EXISTS ${PG_DB_OLD}"

# Export dumps
cd ../api/
npm run dump
cd ../db/

end=`date +%s`

date
echo "Done in $((end-start)) seconds"
