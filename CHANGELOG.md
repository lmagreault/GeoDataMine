# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Next version (to be released)

### Changed
- Update cycleway theme to use latest scheme (Aménagements cyclables 0.3.3) and make use of `designation=greenway` and `traffic_sign=FR:C115` for _voies vertes_
- Update charging station to use latest scheme (IRVE 2.0.0)
- Update carpool theme to use latest scheme (Lieux de covoiturage 0.2.1)
- Update bicycle parking theme to use Etalab scheme (Stationnement cyclable 1.1.0)
- Update taxi theme to use Etalab scheme (Stations de taxi 0.1.1)
- Update address theme to use AITF Base adresse locale 1.3 scheme
- Update shop/craft/office theme to remove ÇaResteOuvert URL and add professional references (CNC, LaPoste, FINESS...)
- Add last_update field into power_supports and shop/craft/office theme

### Fixed
- Database renaming was failing if next DB was being used during process
- Display a pointer cursor over boundary suggestions list


## 0.2.6 - 2021-08-17

### Added
- Theme for taxi stations
- Theme for power and utility poles (thanks to François Lacombe)

### Changed
- Update address theme to use AITF Base adresse locale 1.2 scheme
- Update cycleway theme to use Étalab Aménagements cyclables 0.3.0 scheme

### Fixed
- Database script was broken if configuration file contained a `LICENSE` property with special characters
- City search finds correctly short city names


## 0.2.5 - 2021-02-18

### Added
- Command for themes dump (`npm run dump`) can take an argument to only dump a specific theme (example `npm run dump fuel`)
- Exported datasets contain a `license.txt` file with OSM license details

### Changed
- Updated theme for shop/craft/office (add various kind of shop-like amenities, restrict offices list)

### Fixed
- Dump for theme are always using most complete geometry
- Cycleway theme is using correctly national schema (corrected categorization)
- Some warnings from ogr2ogr were not ignored correctly


## 0.2.4 - 2020-12-17

### Changed
- Updated theme for AED (using new tags after release of GeoDAE database)
- Updated theme for recycling (now wider waste management with `amenity=waste_disposal` features)
- Updated theme for healthcare (includes nursing homes when under `amenity=social_facility` tag)
- Updated theme for cycleways (using Etalab data scheme)
- Updated theme for shop/craft/office (uses `contact:*` OSM tags, link to _Ça reste ouvert_ website)


## 0.2.3 - 2020-07-09

### Added
- Theme for cinemas

### Changed
- Added contact information in various themes (cinema, hosting, public_service, restaurant, shop_craft_office, toilets)
- Improve performance of dump exports (single table + several ogr2ogr calls)
- Post-process operations on database moved from `db/run.sh` to `db/post_process.sql`

### Fixed
- URL in metadata file for dump exports
- Exclude cities along national border (side-effect of raw PBF dump borders)


## 0.2.2 - 2020-06-25

### Added
- Raw exports of whole database in CSV and GeoJSON in /dump directory


## 0.2.1 - 2020-05-06

### Added
- New themes : toilets, drinking water, recycling

### Changed
- Improvements in charging stations theme (Official and OSM ID distinction, more socket types)
- Update of parking theme to use latest Etalab scheme version


## 0.2.0 - 2020-03-09

### Changed
- Database import uses `osm2pgsql` Flex back-end
- Addresses theme is now able to retrieve street names defined in `associatedStreet` relations


## 0.1.2 - 2020-02-11

### Changed
- Added "ref:EU:EVSE" tag in charging station theme
- Updated defibrillator theme according to changes in ARS DAE schema


## 0.1.1 - 2020-01-07

### Added
- Support of XLSX format
- Changelog file

### Fixed
- Links and some install commands of Readme


## 0.1.0 - 2019-12-30

### Added
- Initial release (UI, backend, various themes)
